#ifndef __Sandbox__
#define __Sandbox__

#include <iostream>

using namespace std;

class Sandbox {
public:
  Sandbox();
  ~Sandbox();
  void run();
};

#endif

