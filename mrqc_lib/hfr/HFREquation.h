#ifndef __HFREquation__
#define __HFREquation__

#include <vector>
#include <iostream>

#include <boost/math/special_functions/spherical_harmonic.hpp>

#include <boost/numeric/ublas/matrix.hpp>
#include <boost/numeric/ublas/io.hpp>

#include "./types.h"

using namespace boost::math;
using namespace boost::numeric::ublas;

class HFREquation {
public:
  int atomOrbitalsCount;
  int electornsCount;
  matrix<double> factors;
  vector<double> gto_alaphas;
  HFREquation();
  void resize();
  void init();
  void calculate();
  void print();
};

#endif

