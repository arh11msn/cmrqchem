#include "./HFREquation.h"

HFREquation::HFREquation() {

}

void HFREquation::resize() {
  this->factors = matrix<double>(this->atomOrbitalsCount, this->electornsCount);
  this->gto_alaphas = vector<double>(this->atomOrbitalsCount);
  this->print();
}

void HFREquation::init() {
  for (int i = 0; i < this->factors.size1(); i++) {
    for (int j = 0; j < this->factors.size2(); j++) {
      this->factors(i, j) = 0.5;
    }
  }
  for (int i = 0; i < this->gto_alaphas.size(); i++) {
    this->gto_alaphas(i) = 1.;
  }
  this->print();
}


void HFREquation::calculate() {
  this->print();
}

void HFREquation::print() {
  std::cout << "\n";
  std::cout << this->factors << "\n";
  std::cout << this->gto_alaphas << "\n";
  std::cout << "\n";
}

