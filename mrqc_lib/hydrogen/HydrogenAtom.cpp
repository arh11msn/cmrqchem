#include "./HydrogenAtom.h"

HydrogenAtom::HydrogenAtom() {
  this->init();
}

HydrogenAtom::~HydrogenAtom() {}

void HydrogenAtom::init() {
  this->mass = 1.;
  this->charge = 1.;
  this->position = CartesianPoint();
}

std::ostream& operator<<(std::ostream& os, const HydrogenAtom atom) {
  os << "HydrogenAtom[";
  os << "mass = " << atom.mass;
  os << ", charge = " << atom.charge;
  os << ", position = " << atom.position;
  os << "]";
  return os;  
}

