#include "./HydrogenData.h"

HydrogenData::HydrogenData() {
  this->init();
}

HydrogenData::~HydrogenData() {}

void HydrogenData::init() {
  this->atom_factors = std::array<double, 2>();
  this->atom_factors[0] = 0.5;
  this->atom_factors[1] = 0.5;
}

std::ostream& operator<<(std::ostream& os, const HydrogenData data) {
  os << "HydrogenData[";
  os << data.atom_factors[0] << ", " << data.atom_factors[1];
  os << "]";
  return os;  
}

