#ifndef __Hydrogen__
#define __Hydrogen__

#include <iostream>
#include <array>

#include "../physics/geometry/CartesianPoint.h"

#include "./HydrogenAtom.h"
#include "./HydrogenData.h"

class Hydrogen {
public:
  Hydrogen(double);
  ~Hydrogen();
  void calculate();
protected:
  double distance;
  HydrogenData data;
  std::array<HydrogenAtom, 2> atoms;
  void init();
  double get_energy();
  friend std::ostream& operator<<(std::ostream&, const Hydrogen);
};

#endif

