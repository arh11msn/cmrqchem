#ifndef __HydrogenAtom__
#define __HydrogenAtom__

#include <iostream>

#include "../physics/geometry/CartesianPoint.h"

class HydrogenAtom {
public:
  HydrogenAtom();
  ~HydrogenAtom();
  double mass;
  double charge;
  CartesianPoint position;
protected:
  void init();
  friend std::ostream& operator<<(std::ostream&, const HydrogenAtom);
};

#endif

