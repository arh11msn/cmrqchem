#ifndef __HydrogenData__
#define __HydrogenData__

#include <iostream>

#include <array>

class HydrogenData {
public:
  std::array<double, 2> atom_factors;
  HydrogenData();
  ~HydrogenData();
protected:
  void init();
  friend std::ostream& operator<<(std::ostream&, const HydrogenData);
};

#endif

