#include "./Hydrogen.h"

Hydrogen::Hydrogen(double distance) {
  this->distance = distance;
  this->init();
}

Hydrogen::~Hydrogen() {}

void Hydrogen::init() {
  this->data = HydrogenData();
  this->atoms = std::array<HydrogenAtom, 2>();
  this->atoms[0] = HydrogenAtom();
  this->atoms[1] = HydrogenAtom();
  this->atoms[0].position = CartesianPoint( - this->distance / 2., 0., 0.);
  this->atoms[1].position = CartesianPoint( + this->distance / 2., 0., 0.);
}

void Hydrogen::calculate() {
  std::cout << "\n\ncalculate\n\n";
}

std::ostream& operator<<(std::ostream& os, const Hydrogen hydrogen) {
  os << "Hydrogen[";
  os << "\n\t" << hydrogen.data;
  os << "\n\t" << hydrogen.atoms[0];
  os << "\n\t" << hydrogen.atoms[1];
  os << "\n]";
  return os;  
}

