#include "./WeightGaussFunction.h"

WeightGaussFunction::WeightGaussFunction() {
  this->weight = 1.;
  this->function = GaussFunction();
}

WeightGaussFunction::~WeightGaussFunction() {}

double WeightGaussFunction::operator()(CartesianPoint point) {
  return this->weight * this->function(point);
}

std::ostream& operator<<(std::ostream& os, const WeightGaussFunction f) {
  os << "WeightGaussFunction(";
  os << "weight = " << f.weight;
  os << ", sigma = " << f.function.sigma;
  os << ", center = " << f.function.center;
  os << ")";
  return os;  
}

