#ifndef __WeightGaussFunction__
#define __WeightGaussFunction__

#include <math.h>

#include <iostream>

#include "../../../physics/geometry/CartesianPoint.h"
#include "../../../calculate/abstract/FloatFunction.h"

#include "./GaussFunction.h"

class WeightGaussFunction: public FloatFunction {
public:
  double weight;
  GaussFunction function;
  WeightGaussFunction();
  ~WeightGaussFunction();
  double operator()(CartesianPoint);
  friend std::ostream& operator<<(std::ostream&, const WeightGaussFunction);
};

#endif

