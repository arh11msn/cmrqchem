#include "GaussFunction.h"

GaussFunction::GaussFunction() {
  this->sigma = 1.;
  this->center = CartesianPoint();
}

GaussFunction::~GaussFunction() {}

double GaussFunction::operator()(CartesianPoint point) {
  double factor = 1./(this->sigma * sqrt(2. * M_PI));
  double power_numerator = CartesianPoint::distance(this->center, point);
  double power_denominator = (2. * this->sigma * this->sigma);
  double power = - power_numerator / power_denominator;
  return factor * exp(power);
}

std::ostream& operator<<(std::ostream& os, const GaussFunction f) {
  os << "GaussFunction(";
  os << "sigma = " << f.sigma;
  os << ", center = " << f.center;
  os << ")";
  return os;  
}

