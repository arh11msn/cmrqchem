#ifndef __GaussFunction__
#define __GaussFunction__

#include <math.h>
#include <iostream>

#include "../../../physics/geometry/CartesianPoint.h"
#include "../../../calculate/abstract/FloatFunction.h"

class GaussFunction: public FloatFunction {
public:
  double sigma;
  CartesianPoint center;
  GaussFunction();
  ~GaussFunction();
  double operator()(CartesianPoint);
  friend std::ostream& operator<<(std::ostream&, const GaussFunction);
};

#endif

