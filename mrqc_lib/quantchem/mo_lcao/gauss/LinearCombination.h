#ifndef __LinearCombination__
#define __LinearCombination__

#include <math.h>
#include <iostream>
#include <vector>

#include "../../../physics/geometry/CartesianPoint.h"
#include "../../../calculate/abstract/FloatFunction.h"

#include "./WeightGaussFunction.h"

class LinearCombination: public FloatFunction {
public:
  std::vector<WeightGaussFunction> functions;
  LinearCombination();
  ~LinearCombination();
  double operator()(CartesianPoint);
  void add(WeightGaussFunction);
  friend std::ostream& operator<<(std::ostream&, const LinearCombination);
};

#endif

