#include "LinearCombination.h"

LinearCombination::LinearCombination() {
  this->functions = std::vector<WeightGaussFunction>();
}

LinearCombination::~LinearCombination() {}

void LinearCombination::add(WeightGaussFunction function) {
  this->functions.push_back(function);
}

double LinearCombination::operator()(CartesianPoint point) {
  double value = 0.;
  int size = this->functions.size();
  for (int i = 0; i < size; i++) {
    WeightGaussFunction function;
    function = this->functions[i];
    value += function(point);
  }
  return value;
}

std::ostream& operator<<(std::ostream& os, const LinearCombination f) {
  int size = f.functions.size();
  os << "LinearCombination(size = ";
  os << size << "," << std::endl;
  for (int i = 0; i < size; i++) {
    os << "\t" << f.functions[i] << std::endl;
  }
  os << ")";
  return os;  
}

