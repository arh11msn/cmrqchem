#ifndef __FloatFunction__
#define __FloatFunction__

#include "../../physics/geometry/CartesianPoint.h"

class FloatFunction {
public:
  FloatFunction();
  ~FloatFunction();
  virtual double operator()(CartesianPoint) = 0;
};

#endif

