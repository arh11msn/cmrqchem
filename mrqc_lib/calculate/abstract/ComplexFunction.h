#ifndef __ComplexFunction__
#define __ComplexFunction__

#include <complex>

#include "../../physics/geometry/CartesianPoint.h"

class ComplexFunction {
public:
  ComplexFunction();
  ~ComplexFunction();
  virtual std::complex<double> operator()(CartesianPoint) = 0;
};

#endif

