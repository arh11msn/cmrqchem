#include "CartesianPoint.h"

CartesianPoint::CartesianPoint() {
  this->set(0., 0., 0.);
}

CartesianPoint::CartesianPoint(double x, double y, double z) {
  this->set(x, y, z);
}

CartesianPoint::~CartesianPoint() {}

void CartesianPoint::set(double x, double y, double z) {
  this->x = x;
  this->y = y;
  this->z = z;
}

double CartesianPoint::distance(CartesianPoint p1, CartesianPoint p2) {
  double distance;
  double x_distance = (p1.x - p2.x) * (p1.x - p2.x);
  double y_distance = (p1.y - p2.y) * (p1.y - p2.y);
  double z_distance = (p1.z - p2.z) * (p1.z - p2.z);
  distance = sqrt(x_distance + y_distance + z_distance);
  return distance;
}

std::ostream& operator<<(std::ostream& os, const CartesianPoint point) {
  os << "CartesianPoint[";
  os << "x = " << point.x;
  os << ", y = " << point.y;
  os << ", z = " << point.z;
  os << "]";
  return os;  
}

