#ifndef __CartesianPoint__
#define __CartesianPoint__

#include <math.h>
#include <iostream>

class CartesianPoint {
public:
  double x;
  double y;
  double z;
  CartesianPoint();
  CartesianPoint(double, double, double);
  ~CartesianPoint();
  void set(double, double, double);
  static double distance(CartesianPoint, CartesianPoint);
  friend std::ostream& operator<<(std::ostream&, const CartesianPoint);
};

#endif

