#ifndef __Atom__
#define __Atom__

#include "../geometry/CartesianPoint.h"

class Atom {
public:
  CartesianPoint position;
  Atom();
  ~Atom();
};

#endif

