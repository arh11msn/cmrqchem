#include <iostream>

#include "../../mrqc_lib/hydrogen/Hydrogen.h"
#include "../../mrqc_lib/hfr/HFREquation.h"

int main(int argn, char **argv) {
  std::cout << "\n\n";
  HFREquation eq = HFREquation();
  eq.atomOrbitalsCount = 2;
  eq.electornsCount = 1;
  eq.resize();
  eq.init();
  std::cout << "\n\n";
  return 0;
}

