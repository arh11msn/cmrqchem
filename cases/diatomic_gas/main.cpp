#include <iostream>

#include "../../mrqc_lib/physics/systems/diatomic_gas/DiatomicGas.h"
#include "../../mrqc_lib/physics/geometry/CartesianPoint.h"

int main(int argn, char **argv) {
  CartesianPoint cp = CartesianPoint(1., 1., 1.);
  std::cout << "\n\n\n" << cp << "\n\n\n";
  // DiatomicGas d = DiatomicGas();
  return 0;
}

